/*
 *	procfs.c -	Пример создания файла в /proc, который доступен как на чтение, так и 
на запись.
 */
#include <linux/module.h>	/* Необходимо для любого модуля */
#include <linux/kernel.h>	/* Printk */
#include <linux/proc_fs.h>	/* Необходимо для работы с файловой системой /proc */
#include <asm/uaccess.h>	/* определения функций get_user и put_user */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("NA");

#define MESSAGE_LENGTH 80
static char user_input[MESSAGE_LENGTH];
static struct proc_dir_entry *proc_file;

#define PROC_ENTRY_FILENAME "parrot"

/* Функция чтение из /proc */
/* put_user - передача из пр-ва ядра в пр-во пользователя */
static ssize_t module_output(struct file *filp,
	char *buffer,		/* буфер с данными */
	size_t length,		/* размер буфера */
	loff_t * offset)
{
	static int finished = 0;
	int i;
	char message[MESSAGE_LENGTH + 30];
	if (finished) {
		finished = 0;
		return 0;
	}

	sprintf(message, "Last input: %s\n", user_input);
	for (i = 0; i < length && message[i]; i++)
		put_user(message[i], buffer + i);

	finished = 1;

	return i;
}

/* Функция записи В /proc */
/* Посимвольное считывание из пространства пользователя
 * в пространство ядра с помощью get_user */
static ssize_t
module_input(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	int i;
	for (i = 0; i < MESSAGE_LENGTH - 1 && i < len; i++)
		get_user(user_input[i], buff + i);

	user_input[i] = '\0';
	return i;
}

static struct file_operations file_ops = {
	.read = module_output,		// указатель на функцию
	.write = module_input,		// указатель на функцию
	.owner = THIS_MODULE
};

int __init init_parrot_module(void)	// функция загрузки модуля
{
	int rv = 0;

	// создание файла в proc с правами rw для всех
	// NULL - создаем в корне ФС /proc
	proc_file = proc_create(PROC_ENTRY_FILENAME, 0666, NULL, &file_ops);

	if (proc_file == NULL) {
		rv = -ENOMEM;
		proc_remove(proc_file);
		printk(KERN_ERR "Parrot: Error: Could not create proc-entry\n");
	}
	printk(KERN_INFO "Parrot: Info: Module loaded succesfully\n");
	return rv;
}

void __exit exit_parrot_module(void)	// функция выгрузки модуля
{
	proc_remove(proc_file);	// удаление файла из файловой системы proc
	printk(KERN_INFO "Parrot: Module unloaded succesfully\n");
	return;
}

module_init(init_parrot_module);
module_exit(exit_parrot_module);

